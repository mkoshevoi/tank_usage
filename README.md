# tank_usage

Yandex tank usage. For testing osm-server
------------------------------------------

```
Пропишем алиас в .bashrc
alias yandex-tank-docker='docker run -v /root/config:/var/loadtest -v /root/.ssh:/root.ssh -it direvius/yandex-tank'



yandex-tank-docker -c load1.yaml #Запускаем танк

cat load1.yaml  # фаил конфига танка
overload:
  enabled: true
  package: yandextank.plugins.DataUploader
  token_file: "token.txt"
  job_name: test
phantom:
  address: 89.208.20.126:7070 # [Target's address]:[target's port]
  uris:
    - /search.php?q=moscow+kremlin&polygon_geojson=1&viewbox=
  load_profile:
    load_type: rps # schedule load by defining requests per second
    schedule: line(5, 25, 5m)  # const(2, 3m) # starting from 1rps growing linearly to 10rps during 10 minutes
console:
  enabled: true # enable console output
telegraf:
  enabled: false # let's disable telegraf monitoring for the first time



cat token.txt  # ключ для статистики yandex overload
8eec01f69a1144bd91a3f347c153a151

```